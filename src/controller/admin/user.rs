use hypers::prelude::{hypers_openapi, openapi, Error, OpenApi, Responder};

pub struct User;

#[openapi(name = "user")]
impl User {
    #[get("/index")]
    async fn index() -> impl Responder {}

    #[get("/list")]
    async fn list() -> impl Responder {}

    #[get("/detail")]
    async fn detail() -> impl Responder {}

    #[get("/create")]
    async fn create() -> impl Responder {}

    #[post("/create")]
    async fn create_save() -> impl Responder {}

    #[get("/update")]
    async fn update() -> impl Responder {}

    #[post("/update")]
    async fn update_save() -> impl Responder {}

    #[post("/status")]
    async fn update_status() -> impl Responder {}

    #[get("/update-password")]
    async fn update_password() -> impl Responder {}

    #[post("/update-password")]
    async fn update_password_save() -> impl Responder {}

    #[post("/delete")]
    async fn delete() -> impl Responder {}
}
