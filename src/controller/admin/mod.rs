pub mod article;
pub mod auth;
pub mod user;
pub mod profile;
pub mod upload;
pub mod attach;
pub mod category;
pub mod comment;
pub mod tag;
pub mod page;
pub mod guestbook;
pub mod friendlink;

use super::AppState;
use crate::APP_STATE;
use user::User;
use auth::Auth;
use hypers::prelude::{openapi, Router,Responder,hypers_openapi,Error,OpenApi,Request};

pub fn router_admin(root: &mut Router){
    root.push(User)
        .push(Auth)
        // .push(SysDictData)
        // .push(SysPost)
        // .push(SysDept)
        // .push(SysRole)
        // .push(SysMenu)
        // .push(SysLoginLog)
        // .push(SysUserOnline)
        // .push(SysJob)
        // .push(SysJobLog)
        // .push(SysOperLog)
        // .push(SysMonitor)
        // .push(SysUpdateLog)
        // .push(NoAuthApi)
        ;
}

pub struct Index;

#[openapi]
impl Index {
    #[get("/")]
    async fn index(req: &mut Request) -> impl Responder {
        let mut state = req.get::<AppState>(APP_STATE).unwrap();
    }
}
