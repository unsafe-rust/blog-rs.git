pub mod rsa;
pub mod embed;

use data_encoding::BASE64;
use bcrypt::{DEFAULT_COST, hash, verify};

pub fn format_lensize(size: u64) -> String {
    let sizes = ["", "k", "M", "G", "T", "P", "E", "Z"];
    let mut size: f64 = size as f64;
    let mut count = 0;
    while count < sizes.len() - 1 && size > 1000.0 {
        size /= 1000.0;
        count += 1;
    }
    format!("{:.2}{}", size, sizes[count])
}

// base64 编码
pub fn base64_encode(data: &[u8]) -> String {
    BASE64.encode(data)
}

// base64 解码
pub fn base64_decode(data: String) -> Vec<u8> {
    let data = data.as_bytes();
    let res = BASE64.decode(data).unwrap_or_default();
    res
}

// 加密秘密
pub fn password_hash(password: &str) -> String {
    let hashed: String = hash(password, DEFAULT_COST).unwrap_or("".to_string());
    hashed
}

// 验证密码
pub fn password_verify(password: &str, hash: &str) -> bool {
    let res: bool = verify(password, hash).unwrap_or(false);
    res
}