mod config;
mod controller;
mod dto;
mod entity;
mod middleware;
mod service;
mod util;

use crate::{controller::router,config::CFG};
use hypers::prelude::Result;

pub const APP_STATE: &str = "app_state";

#[tokio::main]
async fn main() -> Result<()> {
    let root = router();
    println!("root = {:#?}",root);
    let listener = hypers::TcpListener::bind(&CFG.server.address).await?;
    println!("Vist browser http://{}/swagger-ui/",&CFG.server.address);
    hypers::listen(root, listener).await
}
