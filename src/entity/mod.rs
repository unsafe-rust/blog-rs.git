use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct Article {
    pub id: u32,
    pub uuid: String,
    pub cate_id: u32,
    pub user_id: u32,
    pub title: String,
    pub keywords: Option<String>,
    pub description: Option<String>,
    pub cover: Option<String>,
    pub content: String,
    pub brief: Option<String>,
    pub tags: Option<String>,
    pub from: Option<String>,
    pub views: Option<u64>,
    pub is_top: Option<i32>,
    pub status: Option<i32>,
    pub add_time: Option<i64>,
    pub add_ip: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Attach {
    pub id: u32,
    pub name: String,
    pub path: String,
    pub ext: String,
    pub size: u64,
    pub md5: String,
    pub r#type: i32,
    pub status: i32,
    pub add_time: i64,
    pub add_ip: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Category {
    pub id: u32,
    pub pid: u32,
    pub name: String,
    pub slug: String,
    pub desc: Option<String>,
    pub sort: i32,
    pub list_tpl: String,
    pub view_tpl: String,
    pub status: Option<i32>,
    pub add_time: Option<i64>,
    pub add_ip: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Comment {
    pub id: u32,
    pub art_id: u32,
    pub reply_id: Option<u32>,
    pub username: String,
    pub email: Option<String>,
    pub content: String,
    pub status: Option<i32>,
    pub add_time: Option<i64>,
    pub add_ip: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Friendlink {
    pub id: u32,
    pub title: String,
    pub url: String,
    pub target: Option<String>,
    pub icon: Option<String>,
    pub sort: Option<i32>,
    pub status: Option<i32>,
    pub add_time: Option<i64>,
    pub add_ip: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Guestbook {
    pub id: u32,
    pub name: String,
    pub message: String,
    pub phone: Option<String>,
    pub email: Option<String>,
    pub qq: Option<String>,
    pub weixin: Option<String>,
    pub status: Option<i32>,
    pub add_time: Option<i64>,
    pub add_ip: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Page {
    pub id: u32,
    pub user_id: u32,
    pub slug: String,
    pub title: String,
    pub keywords: Option<String>,
    pub description: Option<String>,
    pub content: String,
    pub tpl: Option<String>,
    pub status: Option<i32>,
    pub add_time: Option<i64>,
    pub add_ip: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Setting {
    pub id: u32,
    pub key: String,
    pub value: String,
    pub desc: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Tag {
    pub id: u32,
    pub name: String,
    pub desc: Option<String>,
    pub sort: i32,
    pub status: Option<i32>,
    pub add_time: Option<i64>,
    pub add_ip: Option<String>,
}

#[derive(Debug, Deserialize, Serialize, Default)]
pub struct User {
    pub id: u32,
    pub username: String,
    pub password: Option<String>,
    pub nickname: String,
    pub avatar: Option<String>,
    pub sign: Option<String>,
    pub status: Option<i32>,
    pub add_time: Option<i64>,
    pub add_ip: Option<String>,
}
