use hypers::prelude::{hypers_openapi, Extract, ToSchema};
use serde::Deserialize;

#[derive(Deserialize, Clone, Extract, ToSchema, Default, Debug)]
#[extract(body)]
pub struct LoginParams {
    pub name: String,
    pub password: String,
    pub captcha: String,
}
