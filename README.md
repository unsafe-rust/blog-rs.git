## blog-rs 博客系统

`blog-rs` 是使用 `hypers`、 `rbatis` 、 `tera` 等 `rust库` 开发的 `rust` 博客系统

### 项目介绍

*  使用 `rust` 开发的通用博客系统
*  核心使用 `hypers`、 `rbatis` 、 `tera` 等开发
*  博客后台使用 `pear-admin` 后端模板，非前后端分离项目
*  打包静态文件, 模板文件及配置文件。只需一个文件即可部署

### 环境要求

 - rust
 - cargo
 - Myql
 - Redis

### 安装及开发步骤

1. 首先克隆项目到本地

```shell
git clone https://gitee.com/unsafe-rust/blog-rs.git
```

2. 然后配置数据库等信息

```shell
/assert/config/config.yaml
```

3. 最后导入 sql 数据到数据库

```shell
blog.sql
```

4. 运行测试

```shell
cargo run
```

6. 后台登录账号及密码：`admin` / `123456`, 后台登录地址: `/admin/index`


### 特别鸣谢

感谢以下的项目,排名不分先后

 - hypers

 - rbatis
 
 - tera


### 开源协议

*  `blog-rs` 遵循 `Apache2` 开源协议发布，在保留本系统版权的情况下提供个人及商业免费使用。


### 版权

*  该系统所属版权归 [unsafe-rust](https://gitee.com/unsafe-rust) 所有。
